# -*- coding: UTF-8 -*-
__author__  = "samssrus"
__version__ = "0.1.0"
u"""Сборник часто используемых функций"""

import os.path

#устанавливаем base_path
base_path = os.path.dirname(os.path.realpath(__file__))

def tuni(obj, encoding='utf-8'):
	u"""функция приведения строк в юникод"""
	if isinstance(obj, basestring):
		if not isinstance(obj, unicode):
			obj = unicode(obj, encoding)
	return obj

